# Mango Flan

Forked from Flan Scan  https://github.com/cloudflare/flan

Originally:
> Flan Scan is a lightweight network vulnerability scanner. With Flan Scan you can easily find open ports on your network, identify services and their version, and get a list of relevant CVEs affecting your network.

> Flan Scan is a wrapper over Nmap and the vulners script which turns Nmap into a full-fledged network vulnerability scanner. Flan Scan makes it easy to deploy Nmap locally within a container, push results to the cloud, and deploy the scanner on Kubernetes.


## Changes
1. Remove docker requirement
1. Remove Upload options
1. Output to `tex` and convert to `html` with pandoc.
1. Format python with `black`
1. Exclude ports 80 and 22.

## Excluded Ports
There are always CVEs open against ssh/http servers.
The best strategy to address these CVEs is to aggressively patch.
When kept up to date, it is better to not have the long list of CVEs
obscure a potential vulnerability on an uncommon port.

## Run

1. Install the python dependencies in `requirements.txt`
1. Install nmap and pandoc
1. Update `shared/ips.txt` with the list of Addresses to scan
1. ./run.sh 
