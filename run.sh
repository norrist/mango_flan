#!/bin/sh

current_time=$(date "+%Y.%m.%d-%H.%M")

root_dir=shared/
report_extension="tex"
# report_extension="html"

if [[ ! -z $format ]]
then
    report_extension=$format
fi

xml_dir=xml_files/$current_time
report_file=reports/report_$current_time.$report_extension



function get_filename(){
    echo $1 | tr / -
}

mkdir $root_dir$xml_dir
while IFS= read -r line
do
  current_time=$(date "+%Y.%m.%d-%H.%M.%S")
  filename=$(get_filename $line)".xml"
  nmap -sV -oX $root_dir$xml_dir/$filename -oN - -v1 $@ --script=nmap-vulners/vulners.nse $line --exclude-ports="22,80"
#   upload $xml_dir/$filename
done < shared/ips.txt

python3 output_report.py $root_dir$xml_dir $root_dir$report_file shared/ips.txt
pandoc -s  $root_dir$report_file -o report.html 
if [[ $report_extension = "tex" ]]
then
    sed -i 's/_/\\_/g' $root_dir$report_file
    sed -i 's/\$/\\\$/g' $root_dir$report_file
    sed -i 's/#/\\#/g' $root_dir$report_file
    sed -i 's/%/\\%/g' $root_dir$report_file
fi
# upload $report_file
